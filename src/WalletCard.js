import React, {useState} from "react";
import {ethers} from 'ethers'
import axios from "axios";
import Web3 from "web3";
import {Session} from "@heroiclabs/nakama-js";

const WalletCard = () => {
    const [errorMessage, setErrorMessage] = useState(null);
    const [defaultAccount, setDefaultAccount] = useState(null);
    const [userBalance, setUserBalance] = useState(null);
    const [session, setSession] = useState(null);
    const [connButtonText, setConnButtonText] = useState('Connect Wallet');

    const connectWalletHandler = () => {
        if (window.ethereum && window.ethereum.isMetaMask) {
            console.log('MetaMask Here!');
            window.ethereum.request({method: 'eth_requestAccounts'})
                .then(result => {
                    accountChangedHandler(result[0]);
                    setConnButtonText('Wallet Connected');
                    getSessions(result[0])
                })
                .catch(error => {
                    setErrorMessage(error.message);
                });

        } else {
            console.log('Need to install MetaMask');
            setErrorMessage('Please install MetaMask browser extension to interact');
        }
    }
    const accountChangedHandler = (newAccount) => {
        setDefaultAccount(newAccount);
        getAccountBalance(newAccount.toString());
    }
    const getNonce = (account) => {
        return axios.get("http://127.0.0.1:8081/api/v1/nonce/" + account)

    }
    const signMessage = (message, account) => {
        let web3 = new Web3(window.ethereum)
        return web3.eth.personal.sign(message, account, '')
    }
    const authorization = (sign, account) => {
        return axios.post("http://127.0.0.1:8081/api/v1/users", {
            "signature": sign,
            "address": account
        })
    }
    const getSessions = (account) => {
        getNonce(account).then(r => {
            const nonce = r.data.nonce
            let message = nonce + ":" + account
            signMessage(message, account).then(sign => {
                authorization(sign.slice(2), account).then(r =>
                    setSession(new Session(r.data.token || "", r.data.refresh_token || "",  true)))
            })
        })
    }

    const getAccountBalance = (account) => {
        window.ethereum.request({method: 'eth_getBalance', params: [account, 'latest']})
            .then(balance => {
                setUserBalance(ethers.utils.formatEther(balance));
            })
            .catch(error => {
                setErrorMessage(error.message);
            });
    };
    const chainChangedHandler = () => {
        window.location.reload();
    }

    window.ethereum.on('accountsChanged', accountChangedHandler);
    window.ethereum.on('chainChanged', chainChangedHandler);
    return (
        <div className='walletCard'>
            <h4> {"Connection to MetaMask using window.ethereum methods"} </h4>
            <button onClick={connectWalletHandler}>{connButtonText}</button>
            <div className='accountDisplay'>
                <h3>Address: {defaultAccount}</h3>
            </div>
            <div className='balanceDisplay'>
                <h3>Balance: {userBalance}</h3>
            </div>
            <div >
                <h3>Balance: { session? session.token: ''}</h3>
            </div>
            {errorMessage}
        </div>
    )
}
export default WalletCard